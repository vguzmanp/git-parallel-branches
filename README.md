git parallel branches
==============
See changes from different branches in the remote, inside your editor.

Why?
-----
I'm working on branch A (origin/A)
A colleage is working on branch B (origin/B)
We both touch file F
I could see what shape their changes are taking, and avoid re-doing the same work, or reach an agreement on a set of refactorings we need to do.

How?
---

###Dependencies
This is a Python3 project.
We use [pipenv](https://docs.pipenv.org/) to manage dependencies. Just run:

```
pipenv install --dev
pipenv shell
```

###Tests
Run pytest from the root directory:
```
pipenv run python -m pytest
```

###Sublime Plugin
The code inside `sublime-package` is a plugin to show the remote diff in a sidebar.
To run the package:
- Copy `package.py` to the packages folder (`/home/victor/.config/sublime-text-3/Packages/User`)
- Copy `git_rdiff` folder to the same folder, next to `package.py`
- Open the Sublime Console, the plugin should be reloaded automatically (`reloading plugin User.package`)
- Run `view.run_command('example')`
You should see a new tab with a diff:
![](sublime-package/preview_plugin.png)