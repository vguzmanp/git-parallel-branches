import subprocess


class GitRemoteDiff(object):
    '''
    Get a diff for all branches touching a specified path in the remote.
    '''

    remotes = ['origin']

    def __init__(self, repodir):
        super(GitRemoteDiff, self).__init__()
        self.work_dir = repodir

    def git(self, *args):
        argslist = ['git']
        if self.work_dir:
            argslist += ['-C', self.work_dir]
        argslist += list(args)
        output = subprocess.check_output(argslist, universal_newlines=True)
        return output.strip().splitlines()

    def get_remote_branches(self):
        '''
        Get branches in the remotes.
        '''
        remote_branches = []
        for remote in self.remotes:
            remote_refs = self.git('ls-remote', '-h', '--refs', remote)
            for branch in remote_refs:
                branch_name = branch.split('\t')[1].replace('refs/heads/', '').strip()
                remote_branches.append(remote + '/' + branch_name)

        return remote_branches

    def get_touching_branches(self, branches, path):
        '''
        Return those branches that touch the specified path.
        Fetch first.
        '''
        self.git('fetch')
        touching_branches = []
        for br in branches:
            found = False
            cherries = self.git('cherry', 'HEAD', br)
            for c in cherries:
                if found:
                    break
                c_split = c.split()
                flag = c_split[0]
                if flag != '+':
                    continue
                commit_hash = c_split[1]
                # if the hash touches the path
                touched_files = self.git('diff-tree', "--no-commit-id", "--name-only", "-r", commit_hash)
                if path in touched_files:
                    touching_branches.append(br)
                    found = True

        return touching_branches

    def diff(self, branch, path):
        return '\n'.join(self.git('diff', '--color=never', 'HEAD', branch, '--', path))
