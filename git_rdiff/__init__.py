from .git_rdiff import GitRemoteDiff

__all__ = [GitRemoteDiff]
