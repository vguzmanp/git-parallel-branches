import os
import pytest
import subprocess
from git_rdiff import GitRemoteDiff

class TestGitRemoteDiff(object):
    expected_remote_branches = ['origin/ab', 'origin/ac', 'origin/bb', 'origin/master']

    @pytest.fixture(autouse=True)
    def setup(self, tmpdir):
        self.tmpdir = tmpdir.strpath
        print(self.tmpdir)

        cloner = GitRemoteDiff(None)
        cloner.git('clone', 'data/repo_origin', self.tmpdir)
        self.remoteDiff = GitRemoteDiff(self.tmpdir)

    def test_remote_branches(self):
        branches = self.remoteDiff.get_remote_branches()
        print(branches)
        assert branches == self.expected_remote_branches

    def test_touching_branches(self):
        tbranches = self.remoteDiff.get_touching_branches(self.expected_remote_branches, 'a')
        print(tbranches)

        # HEAD is master, it doesn't appear in the output
        assert tbranches == ['origin/ab', 'origin/ac']

    def test_touching_branches_otherpath(self):
        tbranches = self.remoteDiff.get_touching_branches(self.expected_remote_branches, 'b')
        print(tbranches)

        assert tbranches == ['origin/bb']

    def test_touching_branches_nonexisting(self):
        tbranches = self.remoteDiff.get_touching_branches(self.expected_remote_branches, 'notexistingpath')
        print(tbranches)

        assert tbranches == []

    def test_touching_branches_nobranches(self):
        tbranches = self.remoteDiff.get_touching_branches([], 'whatever')
        print(tbranches)

        assert tbranches == []

    def test_touching_branches(self):
        diff = self.remoteDiff.diff('origin/ab', 'a')
        print(diff)

        assert '-A' in diff
        assert '+B' in diff
