import sublime
import sublime_plugin
import tempfile
import subprocess
from .git_rdiff import GitRemoteDiff


class ExampleCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        window = sublime.active_window()
        view = window.new_file()
        view.set_scratch(True)
        set_active_group(window, view, 'right')
        with tempfile.TemporaryDirectory() as temp_dir:
            subprocess.check_output(['git', 'clone', '/home/victor/Development/git-parallel-branches/data/repo_origin', temp_dir])
            remoteDiff = GitRemoteDiff(temp_dir)
            # branches = self.remoteDiff.get_remote_branches()
            # tbranches = self.remoteDiff.get_touching_branches(self.expected_remote_branches, 'b')
            diff = remoteDiff.diff('origin/ab', 'a')
            view.insert(edit, 0, diff)


def set_active_group(window, view, other_group):
    nag = window.active_group()
    if other_group:
        group = 0 if other_group == 'left' else 1
        groups = window.num_groups()
        if groups == 1:
            width = calc_width(view)
            cols = [0.0, width, 1.0] if other_group == 'left' else [0.0, 1-width, 1.0]
            window.set_layout({"cols": cols, "rows": [0.0, 1.0], "cells": [[0, 0, 1, 1], [1, 0, 2, 1]]})
        elif view:
            # group = get_group(groups, nag)
            print("Not Implemented yet")
        window.set_view_index(view, group, 0)
    else:
        group = nag

    # when other_group is left, we need move all views to right except FB view
    if nag == 0 and other_group == 'left' and group == 0:
        for v in reversed(window.views_in_group(nag)[1:]):
            window.set_view_index(v, 1, 0)

    return (nag, group)


def calc_width(view):
    '''
    return float width, which must be
        0.0 < width < 1.0 (other values acceptable, but cause unfriendly layout)
    used in show.show() and "dired_select" command with other_group=True
    '''
    width = view.settings().get('dired_width', 0.3)
    if isinstance(width, float):
        width -= width//1  # must be less than 1
    elif isinstance(width, int if ST3 else long):  # assume it is pixels
        wport = view.viewport_extent()[0]
        width = 1 - round((wport - width) / wport, 2)
        if width >= 1:
            width = 0.9
    else:
        sublime.error_message(u'FileBrowser:\n\ndired_width set to '
                              u'unacceptable type "%s", please change it.\n\n'
                              u'Fallback to default 0.3 for now.' % type(width))
        width = 0.3
    return width or 0.1  # avoid 0.0
